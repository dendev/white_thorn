<?php

return [
    'list_blank_msg' => 'Empty list',
    'list_info_msg' => 'General infos',

    'create_title' => 'Create expense',
    'create_btn_label' => 'Create',
    'create_form_amount_label' => 'Amount',
    'create_form_accounting_date_label' => 'Accounting Date',
    'create_form_type_label' => 'Type',
    'create_form_emitter_label' => 'Emitter',
    'create_form_receiver_label' => 'Receiver',
    'create_form_status_label' => 'Status',
    'create_form_remark_label' => 'Remark',
    'create_form_controls_save_label' => 'Save',
    'create_form_controls_cancel_label' => 'Cancel',

    'checking_to_paid_success' => 'Ok its paid',
    'checking_to_paid_error' => 'Error during paid process',
    'checking_to_unpaid_success' => 'Ok its unpaid',
    'checking_to_unpaid_error' => 'Error during unpaid process',
];
