<?php

return [
    'footer_infos' => "Ceci est un mail automatique.",
    'footer_copyright' => "Ⓒ :year <a href='https://www.viriditas.be'>Viriditas</a>. Tous droits réservés.",

    'receiver_expense_subject' => 'Note de frais',
    'receiver_expense_content' => "Bonjour, <br><br>voici la note de frais du type ':type' pour la date du :date.<br><br>Elle concerne :emitter_name ( :emitter_bank_account ) pour un montant de :amount euros.",
    'receiver_expense_title' => 'Note de frais',
    'receiver_expense_btn_label' => 'no',

    'emitter_checking_open_subject' => "Note de frais à vérifier",
    'emitter_checking_open_title' => "Vérification du paiement",
    'emitter_checking_open_content' => "Bonjour, <br><br>une note de frais est arrivée à échéance, elle devrait donc être payer. Merci de vérifier si elle est bien payé ou pas.",
    'emitter_checking_open_btn_label' => "Vérification",
];
