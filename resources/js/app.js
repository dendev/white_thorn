import './bootstrap';
import './bulma/tabs';
import Bulma from '@vizuaalog/bulmajs';
import Alpine from 'alpinejs';

window.Alpine = Alpine;

if( document.querySelector('#action-modal') )
{
    modal_trigger.addEventListener('click', function(e) {
        let modal = Bulma('.modal').modal();
        modal.open();
    });
}

Alpine.start();
