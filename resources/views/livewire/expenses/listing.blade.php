<section class="expense__listing">
    <!-- create btn -->
    <a href="{{route('pages.expense.create')}}" class="button is-primary is-medium expense__create__btn">{{__('expense.create_btn_label')}}</a>

    <!-- info -->
    <div class="notification is-info mt-5 mb-2">
        {{__('expense.list_info_msg')}}
    </div>

    <!-- list -->
    @if( $is_empty )
            <livewire:expenses.blank></livewire:expenses.blank>
    @else
        @foreach($expense_ids as $expense_id )
            <livewire:expenses.card expense={{$expense_id}}></livewire:expenses.card>
        @endforeach
    @endif


</section>
