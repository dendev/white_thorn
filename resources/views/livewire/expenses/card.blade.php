<div class="card expense__card" id="expense-{{$expense->id}}">
    <header class="card-header">
        <p class="card-header-title expense__card__title">
            <span class="expense__card__title__category">{{$expense->type->label}}</span>
            <span class="expense__card__title__date icon-text">
                <span> @datefr($expense->accounting_date)</span>
                <span class="icon expense__card__title__status" style="color: {{$expense->status->type->color}};">
                    <i class="{{$expense->status->type->icon}}"> </i>
                </span>
            </span>
        </p>
    </header>
    <div class="card-content">
        <div class="content">
            <p><b>@datefr($expense->created_at)</b>, <b>{{$expense->receiver->name}}</b> doit <b>{{$expense->amount}} Euro(s)</b> à <b>{{$expense->emitter->name }}</b></p>
            <time datetime="{{$expense->updated_at}}"><b>@datefr($expense->status->updated_at): </b></time>
            {{ $expense->status->type->label }}
        </div>
    </div>
    <div class="card">
        <footer class="card-footer">
            @can('view', $expense)
                <a href="{{route('pages.expense.show', ['id' => $expense->id])}}" class="card-footer-item hoverable">
                    <span class="icon-text has-text-info">
                          <span class="icon">
                              <i class="fas fa-eye"></i>
                          </span>
                          <span>Détails</span>
                    </span>
                </a>
            @endcan

            @can('verify', $expense)
                    <span class="icon-text has-text-danger card-footer-item clickable hoverable" wire:click="handle_click_unpaid">
                          <span class="icon">
                              <i class="fas fa-xmark"></i>
                          </span>
                          <span>Non payé</span>
                    </span>
                    <span class="icon-text has-text-success card-footer-item clickable hoverable" wire:click="handle_click_paid">
                          <span class="icon">
                              <i class="fas fa-check"></i>
                          </span>
                          <span>Payé</span>
                    </span>
                @endcan
        </footer>
    </div>
</div>
