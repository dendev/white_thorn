<form id="expense__create__form" wire:submit.prevent="submit">
    <!-- amount -->
    <div class="field">
        <label class="label">{{__('expense.create_form_amount_label')}}</label>
        <div class="control">
            <input class="input" id="amount" name="amount" type="number" wire:model="amount" placeholder="{{__('expense.create_form_amount_placeholder')}}" min="1" step=".01">
            @error('amount') <span class="error">{{ $message }}</span> @enderror
        </div>
    </div>

    <!-- accounting_date-->
    <div class="field">
        <label class="label">{{__('expense.create_form_accounting_date_label')}}</label>
        <div class="control">
            <input class="input" id="accounting_date" name="accounting_date" type="date" wire:model="accounting_date" placeholder="{{__('expense.create_form_accounting_date_placeholder')}}">
            @error('accounting_date') <span class="error">{{ $message }}</span> @enderror
        </div>
    </div>

    <!-- type -->
    <div class="field">
        <label class="label">{{__('expense.create_form_type_label')}}</label>
        <div class="control">
            <div class="select w-100">
                <select id="type_id" wire:model="type_id" class="w-100">
                    @foreach( $type_options as $key => $value)
                        <option>{{$value}}</option>
                    @endforeach
                </select>
            </div>
            @error('type_id') <span class="error">{{ $message }}</span> @enderror
        </div>
    </div>

    <!-- emitter -->
    <div class="field">
        <label class="label">{{__('expense.create_form_emitter_label')}}</label>
        <div class="control">
            <div class="select w-100">
                <select id="emitter_id" wire:model="emitter_id" class="w-100">
                    @foreach( $emitter_options as $key => $value)
                        <option value="{{$key}}">{{$value}}</option>
                    @endforeach
                </select>
            </div>
            @error('emitter_id') <span class="error">{{ $message }}</span> @enderror
        </div>
    </div>

    <!-- receiver -->
    <div class="field">
        <label class="label">{{__('expense.create_form_receiver_label')}}</label>
        <div class="control">
            <div class="select w-100">
                <select id="receiver_id" wire:model="receiver_id" class="w-100">
                    @foreach( $receiver_options as $key => $value)
                        <option>{{$value}}</option>
                    @endforeach
                </select>
            </div>
            @error('receiver_id') <span class="error">{{ $message }}</span> @enderror
        </div>
    </div>

    <!-- remarks -->
    <div class="field input-textarea-field">
        <label class="label">{{__('expense.create_form_remark_label')}}</label>
        <div class="control">
            <textarea wire:model="remark" class="textarea" placeholder="{{__('expense.create_form_remark_placeholder')}}"></textarea>
        </div>
    </div>

    <!-- controls -->
    <div class="field is-grouped expense__create__controls">
        <div class="control">
            <button class="button is-link is-light">{{__('expense.create_form_controls_cancel_label')}}</button>
        </div>
        <div class="control">
            <button class="button is-link" type="submit">{{__('expense.create_form_controls_save_label')}}</button>
        </div>
    </div>
</form>
