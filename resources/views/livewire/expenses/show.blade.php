<div class="tabs">
    <ul>
        <li class="tab is-active" onclick="openTab(event,'general')"><a>Général</a></li>
        <li class="tab" onclick="openTab(event,'historic')"><a>Historique</a></li>
        <li class="tab" onclick="openTab(event,'emails')"><a>Emails</a></li>
        <li class="tab" onclick="openTab(event,'pdf')"><a>Pdf</a></li>
    </ul>
</div>

<div class="container section expense__show__tabs">
    <div id="general" class="content-tab expense__show__tabs__tab" >
        <div class="table-container">
            <table class="table" style="width: 100%;">
                <tbody>
                <tr>
                    <td><b>Type</b></td>
                    <td>{{$expense->type->label}}</td>
                </tr>
                <tr>
                    <td><b>Expéditeur</b></td>
                    <td>{{$expense->emitter->name}}</td>
                </tr>
                <tr>
                    <td><b>Destinataire</b></td>
                    <td>{{$expense->receiver->name}}</td>
                </tr>
                <tr>
                    <td><b>Montant</b></td>
                    <td>{{$expense->amount}}</td>
                </tr>
                <tr>
                    <td><b>Crée le</b></td>
                    <td>@datefr($expense->created_at)</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div id="historic" class="content-tab" style="display:none">
        <div class="table-container">
            <table class="table" style="width: 100%;">
                <thead>
                <tr>
                    <th>Date</th>
                    <th>Description</th>
                </tr>
                </thead>
                <tbody>
                @foreach( $expense->statuses as $status)
                    <tr>
                        <td><b>@datefr_with_time($status->pivot->updated_at) </b></td>
                        <td>{{$status->label}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div id="emails" class="content-tab" style="display:none">
        <div class="table-container">
            <table class="table" style="width: 100%;">
                <thead>
                <tr>
                    <th>Date</th>
                    <th>Sujet</th>
                    <th>Destinataire</th>
                    <th>Nb Envoi(s)</th>
                </tr>
                </thead>
                <tbody>
                @foreach( $expense->get_mails() as $mail)
                    <tr>
                        <td><b>@datefr_with_time($mail->updated_at) </b></td>
                        <td>{{$mail->subject}}</td>
                        <td>{{$mail->to}}</td>
                        <td>{{$mail->nb_sent}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div id="pdf" class="content-tab" style="display:none">
        <div class="table-container">
            <table class="table" style="width: 100%;">
                <thead>
                <tr>
                    <th>Date</th>
                    <th>Document</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach( $expense->documents as $document)
                    <tr>
                        <td><b>@datefr_with_time($document->updated_at) </b></td>
                        <td>{{$document->filename}}</td>
                        <td><a href="{{$document->url}}" download>
                                <span class="icon"><i class="fas fa-eye"></i></span>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
