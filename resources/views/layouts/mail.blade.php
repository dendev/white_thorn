<?php //https://www.cerberusemail.com/templates ?>
<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
    <meta charset="utf-8"> <!-- utf-8 works for most cases -->
    <meta name="viewport" content="width=device-width"> <!-- Forcing initial-scale shouldn't be necessary -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- Use the latest (edge) version of IE rendering engine -->
    <meta name="x-apple-disable-message-reformatting">  <!-- Disable auto-scale in iOS 10 Mail entirely -->
    <meta name="format-detection" content="telephone=no,address=no,email=no,date=no,url=no"> <!-- Tell iOS not to automatically link certain text strings. -->
    <meta name="color-scheme" content="light">
    <meta name="supported-color-schemes" content="light">
    <title></title> <!--   The title tag shows in email notifications, like Android 4.4. -->

    <!-- What it does: Makes background images in 72ppi Outlook render at correct size. -->
    <!--[if gte mso 9]>
    <xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
    </xml>
    <![endif]-->

    <!-- Web Font / @font-face : BEGIN -->
    <!-- NOTE: If web fonts are not required, lines 23 - 41 can be safely removed. -->

    <!-- Desktop Outlook chokes on web font references and defaults to Times New Roman, so we force a safe fallback font. -->
    <!--[if mso]>
        <style>
            * {
                font-family: monospace !important;
            }
        </style>
    <![endif]-->

    <!-- All other clients get the webfont reference; some will render the font and others will silently fail to the fallbacks. More on that here: http://stylecampaign.com/blog/2015/02/webfont-support-in-email/ -->
    <!--[if !mso]><!-->
    <!-- insert web font reference, eg: <link href='https://fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'> -->
    <!--<![endif]-->

    <!-- Web Font / @font-face : END -->

    <!-- CSS Reset : BEGIN -->
    <style>

        /* What it does: Tells the email client that only light styles are provided but the client can transform them to dark. A duplicate of meta color-scheme meta tag above. */
        :root {
            color-scheme: light;
            supported-color-schemes: light;
        }

        /* What it does: Remove spaces around the email design added by some email clients. */
        /* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
        html,
        body {
            margin: 0 auto !important;
            padding: 0 !important;
            height: 100% !important;
            width: 100% !important;
        }

        /* What it does: Stops email clients resizing small text. */
        * {
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
        }

        /* What it does: Centers email on Android 4.4 */
        div[style*="margin: 16px 0"] {
            margin: 0 !important;
        }

        /* What it does: forces Samsung Android mail clients to use the entire viewport */
        #MessageViewBody, #MessageWebViewDiv{
            width: 100% !important;
        }

        /* What it does: Stops Outlook from adding extra spacing to tables. */
        table,
        td {
            mso-table-lspace: 0pt !important;
            mso-table-rspace: 0pt !important;
        }

        /* What it does: Fixes webkit padding issue. */
        table {
            border-spacing: 0 !important;
            border-collapse: collapse !important;
            table-layout: fixed !important;
            margin: 0 auto !important;
        }

        /* What it does: Uses a better rendering method when resizing images in IE. */
        img {
            -ms-interpolation-mode:bicubic;
        }

        /* What it does: Prevents Windows 10 Mail from underlining links despite inline CSS. Styles for underlined links should be inline. */
        a {
            text-decoration: none;
        }

        /* What it does: A work-around for email clients meddling in triggered links. */
        a[x-apple-data-detectors],  /* iOS */
        .unstyle-auto-detected-links a,
        .aBn {
            border-bottom: 0 !important;
            cursor: default !important;
            color: inherit !important;
            text-decoration: none !important;
            font-size: inherit !important;
            font-family: inherit !important;
            font-weight: inherit !important;
            line-height: inherit !important;
        }

        /* What it does: Prevents Gmail from displaying a download button on large, non-linked images. */
        .a6S {
            display: none !important;
            opacity: 0.01 !important;
        }

        /* What it does: Prevents Gmail from changing the text color in conversation threads. */
        .im {
            color: inherit !important;
        }

        /* If the above doesn't work, add a .g-img class to any image in question. */
        img.g-img + div {
            display: none !important;
        }

        /* What it does: Removes right gutter in Gmail iOS app: https://github.com/TedGoas/Cerberus/issues/89  */
        /* Create one of these media queries for each additional viewport size you'd like to fix */

        /* iPhone 4, 4S, 5, 5S, 5C, and 5SE */
        @media only screen and (min-device-width: 320px) and (max-device-width: 374px) {
    u ~ div .email-container {
                min-width: 320px !important;
            }
        }
        /* iPhone 6, 6S, 7, 8, and X */
        @media only screen and (min-device-width: 375px) and (max-device-width: 413px) {
    u ~ div .email-container {
                min-width: 375px !important;
            }
        }
        /* iPhone 6+, 7+, and 8+ */
        @media only screen and (min-device-width: 414px) {
    u ~ div .email-container {
                min-width: 414px !important;
            }
        }

    </style>
    <!-- CSS Reset : END -->

    <!-- Progressive Enhancements : BEGIN -->
    <style>

	    /* What it does: Hover styles for buttons */
	    .button-td,
	    .button-a {
            transition: all 100ms ease-in;
	    }
	    .button-td-primary:hover,
	    .button-a-primary:hover {
            background: #485fc7 !important;
            border-color: #485fc7 !important;
	    }

	    /* Media Queries */
	    @media screen and (max-width: 600px) {

    /* What it does: Adjust typography on small screens to improve readability */
    .email-container p {
        font-size: 17px !important;
	        }
	    }

        .magicpattern {
            min-height: 111px;
            width: 100%;
            height: 100%;
            background-size: cover;
            background-position: center center;
            background-repeat: repeat;
            background-image: url("data:image/svg+xml;utf8,%3Csvg viewBox=%220 0 1500 1100%22 xmlns=%22http:%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%3Cmask id=%22b%22 x=%220%22 y=%220%22 width=%221500%22 height=%221100%22%3E%3Cpath fill=%22url(%23a)%22 d=%22M0 0h1500v1100H0z%22%2F%3E%3C%2Fmask%3E%3Cpath fill=%22%23000336%22 d=%22M0 0h1500v1100H0z%22%2F%3E%3Cg style=%22transform-origin:center center%22 stroke=%22%234c4e72%22 stroke-width=%221.35%22 mask=%22url(%23b)%22%3E%3Cpath fill=%22none%22 d=%22M100 0h100v100H100zM200 0h100v100H200zM400 0h100v100H400zM500 0h100v100H500z%22%2F%3E%3Cpath fill=%22%234c4e727b%22 d=%22M600 0h100v100H600z%22%2F%3E%3Cpath fill=%22%234c4e726d%22 d=%22M700 0h100v100H700z%22%2F%3E%3Cpath fill=%22none%22 d=%22M800 0h100v100H800zM900 0h100v100H900zM1000 0h100v100h-100zM1100 0h100v100h-100zM1200 0h100v100h-100z%22%2F%3E%3Cpath fill=%22%234c4e726c%22 d=%22M1300 0h100v100h-100z%22%2F%3E%3Cpath fill=%22none%22 d=%22M1400 0h100v100h-100zM0 100h100v100H0zM100 100h100v100H100zM200 100h100v100H200zM300 100h100v100H300zM600 100h100v100H600z%22%2F%3E%3Cpath fill=%22%234c4e720d%22 d=%22M700 100h100v100H700z%22%2F%3E%3Cpath fill=%22none%22 d=%22M800 100h100v100H800zM1000 100h100v100h-100zM1100 100h100v100h-100zM1200 100h100v100h-100z%22%2F%3E%3Cpath fill=%22%234c4e728d%22 d=%22M1300 100h100v100h-100z%22%2F%3E%3Cpath fill=%22none%22 d=%22M1400 100h100v100h-100zM0 200h100v100H0zM100 200h100v100H100zM200 200h100v100H200zM300 200h100v100H300zM500 200h100v100H500zM600 200h100v100H600zM800 200h100v100H800z%22%2F%3E%3Cpath fill=%22%234c4e72e8%22 d=%22M900 200h100v100H900z%22%2F%3E%3Cpath fill=%22%234c4e72be%22 d=%22M1000 200h100v100h-100z%22%2F%3E%3Cpath fill=%22none%22 d=%22M1100 200h100v100h-100z%22%2F%3E%3Cpath fill=%22%234c4e720f%22 d=%22M1200 200h100v100h-100z%22%2F%3E%3Cpath fill=%22%234c4e7207%22 d=%22M1400 200h100v100h-100z%22%2F%3E%3Cpath fill=%22none%22 d=%22M0 300h100v100H0z%22%2F%3E%3Cpath fill=%22%234c4e72db%22 d=%22M100 300h100v100H100z%22%2F%3E%3Cpath fill=%22none%22 d=%22M200 300h100v100H200zM300 300h100v100H300zM400 300h100v100H400zM500 300h100v100H500z%22%2F%3E%3Cpath fill=%22%234c4e7299%22 d=%22M600 300h100v100H600z%22%2F%3E%3Cpath fill=%22%234c4e72d8%22 d=%22M700 300h100v100H700z%22%2F%3E%3Cpath fill=%22none%22 d=%22M1000 300h100v100h-100zM1100 300h100v100h-100zM1200 300h100v100h-100zM1400 300h100v100h-100zM0 400h100v100H0z%22%2F%3E%3Cpath fill=%22%234c4e723f%22 d=%22M100 400h100v100H100z%22%2F%3E%3Cpath fill=%22%234c4e72af%22 d=%22M200 400h100v100H200z%22%2F%3E%3Cpath fill=%22none%22 d=%22M300 400h100v100H300zM400 400h100v100H400z%22%2F%3E%3Cpath fill=%22%234c4e72de%22 d=%22M500 400h100v100H500z%22%2F%3E%3Cpath fill=%22%234c4e7269%22 d=%22M600 400h100v100H600z%22%2F%3E%3Cpath fill=%22%234c4e7281%22 d=%22M700 400h100v100H700z%22%2F%3E%3Cpath fill=%22none%22 d=%22M800 400h100v100H800zM900 400h100v100H900zM1000 400h100v100h-100zM1100 400h100v100h-100zM1200 400h100v100h-100zM1400 400h100v100h-100zM0 500h100v100H0zM100 500h100v100H100z%22%2F%3E%3Cpath fill=%22%234c4e72c3%22 d=%22M200 500h100v100H200z%22%2F%3E%3Cpath fill=%22none%22 d=%22M300 500h100v100H300zM600 500h100v100H600z%22%2F%3E%3Cpath fill=%22%234c4e72cd%22 d=%22M700 500h100v100H700z%22%2F%3E%3Cpath fill=%22none%22 d=%22M900 500h100v100H900zM1000 500h100v100h-100zM1100 500h100v100h-100zM1200 500h100v100h-100zM1400 500h100v100h-100zM0 600h100v100H0zM100 600h100v100H100z%22%2F%3E%3Cpath fill=%22%234c4e7205%22 d=%22M200 600h100v100H200z%22%2F%3E%3Cpath fill=%22none%22 d=%22M500 600h100v100H500zM600 600h100v100H600zM800 600h100v100H800zM900 600h100v100H900zM1000 600h100v100h-100zM1100 600h100v100h-100z%22%2F%3E%3Cpath fill=%22%234c4e723b%22 d=%22M1200 600h100v100h-100z%22%2F%3E%3Cpath fill=%22none%22 d=%22M0 700h100v100H0zM100 700h100v100H100zM200 700h100v100H200zM300 700h100v100H300z%22%2F%3E%3Cpath fill=%22%234c4e723c%22 d=%22M600 700h100v100H600z%22%2F%3E%3Cpath fill=%22none%22 d=%22M700 700h100v100H700zM800 700h100v100H800zM900 700h100v100H900zM1000 700h100v100h-100z%22%2F%3E%3Cpath fill=%22%234c4e72e4%22 d=%22M1100 700h100v100h-100z%22%2F%3E%3Cpath fill=%22none%22 d=%22M1200 700h100v100h-100zM1400 700h100v100h-100z%22%2F%3E%3Cpath fill=%22%234c4e72da%22 d=%22M0 800h100v100H0z%22%2F%3E%3Cpath fill=%22none%22 d=%22M100 800h100v100H100zM200 800h100v100H200z%22%2F%3E%3Cpath fill=%22%234c4e721e%22 d=%22M300 800h100v100H300z%22%2F%3E%3Cpath fill=%22none%22 d=%22M500 800h100v100H500zM600 800h100v100H600zM700 800h100v100H700z%22%2F%3E%3Cpath fill=%22%234c4e726a%22 d=%22M900 800h100v100H900z%22%2F%3E%3Cpath fill=%22none%22 d=%22M1000 800h100v100h-100zM1400 800h100v100h-100zM0 900h100v100H0zM200 900h100v100H200z%22%2F%3E%3Cpath fill=%22%234c4e723a%22 d=%22M400 900h100v100H400z%22%2F%3E%3Cpath fill=%22none%22 d=%22M500 900h100v100H500zM1000 900h100v100h-100zM1100 900h100v100h-100zM1300 900h100v100h-100z%22%2F%3E%3Cpath fill=%22%234c4e72e8%22 d=%22M1400 900h100v100h-100z%22%2F%3E%3Cpath fill=%22none%22 d=%22M0 1000h100v100H0zM100 1000h100v100H100zM200 1000h100v100H200z%22%2F%3E%3Cpath fill=%22%234c4e72d9%22 d=%22M300 1000h100v100H300z%22%2F%3E%3Cpath fill=%22none%22 d=%22M500 1000h100v100H500z%22%2F%3E%3Cpath fill=%22%234c4e729e%22 d=%22M600 1000h100v100H600z%22%2F%3E%3Cpath fill=%22none%22 d=%22M700 1000h100v100H700zM800 1000h100v100H800zM900 1000h100v100H900zM1000 1000h100v100h-100z%22%2F%3E%3Cpath fill=%22%234c4e7228%22 d=%22M1200 1000h100v100h-100z%22%2F%3E%3Cpath fill=%22none%22 d=%22M1300 1000h100v100h-100z%22%2F%3E%3C%2Fg%3E%3Cdefs%3E%3CradialGradient id=%22a%22%3E%3Cstop offset=%220%22 stop-color=%22%23fff%22%2F%3E%3Cstop offset=%221%22 stop-color=%22%23fff%22%2F%3E%3C%2FradialGradient%3E%3C%2Fdefs%3E%3C%2Fsvg%3E");
        }

        .footer__infos {
            width: 100%;
            justify-content: right;
            padding: 9px 5px 9px 0;
            font-size: x-small;
            color: #4a4a4a;
        }

        .footer__copyright {
            width: 100%;
            justify-content: center;
            padding: 9px 5px 9px 0;
            font-size: x-small;
            color: #4a4a4a;
        }
    </style>
    <!-- Progressive Enhancements : END -->

</head>
<!--
The email background color (#222222) is defined in three places:
    1. body tag: for most email clients
2. center tag: for Gmail and Inbox mobile apps and web versions of Gmail, GSuite, Inbox, Yahoo, AOL, Libero, Comcast, freenet, Mail.ru, Orange.fr
	3. mso conditional: For Windows 10 Mail
-->
<body width="100%" style="margin: 0; padding: 0 !important; mso-line-height-rule: exactly; background-color: #dfe3ee;">
	<center role="article" aria-roledescription="email" lang="fr" style="width: 100%; background-color: #dfe3ee;">
    <!--[if mso | IE]>
    <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color: #dfe3ee;">
    <tr>
    <td>
    <![endif]-->

        <!-- Visually Hidden Preheader Text : BEGIN -->
        <div style="max-height:0; overflow:hidden; mso-hide:all;" aria-hidden="true">
    (Optional) This text will appear in the inbox preview, but not the email body. It can be used to supplement the email subject line or even summarize the email's contents. Extended text preheaders (~490 characters) seems like a better UX for anyone using a screenreader or voice-command apps like Siri to dictate the contents of an email. If this text is not included, email clients will automatically populate it using the text (including image alt text) at the start of the email's body.
        </div>
        <!-- Visually Hidden Preheader Text : END -->

        <!-- Create white space after the desired preview text so email clients don’t pull other distracting text into the inbox preview. Extend as necessary. -->
        <!-- Preview Text Spacing Hack : BEGIN -->
        <div style="display: none; font-size: 1px; line-height: 1px; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden; mso-hide: all; font-family: monospace;">
	        &zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;
        </div>
        <!-- Preview Text Spacing Hack : END -->

        <!--
Set the email width. Defined in two places:
            1. max-width for all clients except Desktop Windows Outlook, allowing the email to squish on narrow but never go wider than 600px.
2. MSO tags for Desktop Windows Outlook enforce a 600px width.
-->
        <div style="max-width: 600px; margin: 0 auto;" class="email-container">
            <!--[if mso]>
            <table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="600">
            <tr>
            <td>
            <![endif]-->

	        <!-- Email Body : BEGIN -->
	        <table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: auto;">
		        <!-- Email Header : BEGIN -->
	            <tr>
	                <td style="padding: 29px 0 20px 0; text-align: left; text-transform: uppercase; font-size: 28px; font-weight: bolder; font-family: monospace">
                        Epine Blanche
	                </td>
	            </tr>
		        <!-- Email Header : END -->

                <!-- 1 Column Text + Button : BEGIN -->
                <tr>
                    <td style="background-color: #ffffff;">
                        <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tr>
                                <td style="padding: 20px; font-family: monospace; font-size: 18px; line-height: 20px; color: #555555;">
                                    <h1 style="margin: 0 0 10px 0; font-family: monospace; font-size: 24px; line-height: 30px; color: #333333; font-weight: normal;">@yield('title')</h1>
                                   <div>
                                       <br>
                                        @yield('content')
                                   </div>
                                </td>
                            </tr>
                            <tr>
                                @hasSection('btn_url')
                                <td style="padding: 0 20px 23px 0;">
                                    <!-- Button : BEGIN -->
                                    <table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" style="margin: auto;">
                                        <tr>
                                            <td class="button-td button-td-primary" style="border-radius: 4px; background: #3b5998;">
											     <a class="button-a button-a-primary" href="@yield('btn_url')" style="background: #3b5998; border: 1px solid #3b5998; font-family: monospace; font-size: 15px; line-height: 15px; text-decoration: none; padding: 13px 17px; color: #ffffff; display: block; border-radius: 4px;">@yield('btn_label')</a>
											</td>
                                        </tr>
                                    </table>
                                    <!-- Button : END -->
                                </td>
                                @endif
                            </tr>
                        </table>
                    </td>
                </tr>
                <!-- 1 Column Text + Button : END -->

                <!-- Clear Spacer : BEGIN -->
                <tr>
                    <td aria-hidden="true" height="40" style="font-size: 0px; line-height: 0px;">
                        &nbsp;
                    </td>
                </tr>
                <!-- Clear Spacer : END -->
            </table>
            <!-- Email Body : END -->

            <!-- Email Footer : BEGIN -->

            <table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: auto; background-color: #d9dbf1" class="">
                <tr>
                    <td colspan="2" style="text-align: end" class="footer__infos">{{__('mail.footer_infos')}}</td>
                </tr>
            </table>

	        <table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: auto; background-color: #d9dbf1" class="magicpattern">
            </table>

            <table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: auto; background-color: #d9dbf1" class="">
                <tr style="text-align: center">
                    <td colspan="2" class="footer__copyright">{!!__('mail.footer_copyright', ['year' => now()->format('Y')])!!}</td>
                </tr>
            </table>
            <!-- Email Footer : END -->

            <!--[if mso]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </div>
    <!--[if mso | IE]>
    </td>
    </tr>
    </table>
    <![endif]-->
    </center>
</body>
</html>
