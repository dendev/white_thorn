<footer class="footer">

    <div class="icon-text has-text-info footer__bug">
        <a href="mailto:contact-project+dendev-white-thorn-43822779-issue-@incoming.gitlab.com?subject=Bugs au sujet de ?&body=Informations au sujet du bug rencontré ....">
          <span class="icon">
                  <i class="fas fa-bug"></i>
          </span>
          <span>Bugs ?</span>
        </a>
    </div>

    <div class="content has-text-centered magicpattern footer-content">
        <p>
        </p>
    </div>

    <div class="has-text-centered footer__copyright">
        <p>&#9400; {{now()->format('Y')}} <a href="{{env('URL_WEBSITE')}}" target="_blank">Viriditas</a>. Tous droits réservés.</p>
    </div>
</footer>
