<nav class="navbar" role="navigation" aria-label="main navigation">
    <div class="navbar-brand">
        <!-- logo -->
        <a class="navbar-item navbar-title" href="{{route('pages.dashboard')}}">
            Epine Blanche
        </a>

        <!-- burger menu -->
        <a role="button" class="navbar-burger" data-target="navMenu" aria-label="menu" aria-expanded="false">
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
        </a>
    </div>

    <div class="navbar-menu" id="navMenu">
        <!-- navbar start, navbar end -->
        <div class="navbar-end">
            <div class="navbar-item">
                <a href="{{route('filament.pages.dashboard')}}" class="field block w-full px-4 py-2 text-left text-sm leading-5 text-gray-700 dark:text-gray-300 hover:bg-gray-100 dark:hover:bg-gray-800 focus:outline-none focus:bg-gray-100 dark:focus:bg-gray-800 transition duration-150 ease-in-out">Admin</a>
            </div>
            <div class="navbar-item">
                <form method="POST" action="{{ route('logout') }}">
                @csrf
                <x-dropdown-link :href="route('logout')"
                onclick="event.preventDefault();
                this.closest('form').submit();">
                {{ __('Log Out') }}
                </x-dropdown-link>
                </form>
            </div>

        </div>
    </div>
</nav>
