@extends('layouts.mail')

@section('title')
    {{__('mail.receiver_expense_title') }}
@endsection()

@section('content')
    {!! $content !!}
@endsection

@section('btn_label')
    {{__('mail.receiver_expense_btn_label') }}
@endsection

<!-- same line to prevent space in url action -->
