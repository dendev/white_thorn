@extends('layouts.mail')

@section('title')
    {{__('mail.emitter_checking_open_title') }}
@endsection()

@section('content')
    {!! $content !!}
@endsection

@section('btn_label')
    {{__('mail.emitter_checking_open_btn_label') }}
@endsection

<!-- same line to prevent space in url action -->
@section('btn_url'){{route('pages.dashboard').'#expense-' . $expense->id}}@endsection
