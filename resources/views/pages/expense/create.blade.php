<x-app-layout>
    <div class="container">
        <livewire:shared.title title={{$title}}></livewire:shared.title>
        <livewire:expenses.create></livewire:expenses.create>
    </div>
</x-app-layout>

