<x-app-layout>
    <div class="container">
        <livewire:shared.title title={{$title}}></livewire:shared.title>
        <livewire:expenses.show expense={{$expense}}></livewire:expenses.show>
    </div>
</x-app-layout>

