<x-app-layout>
    <div class="container">

    <livewire:shared.title title={{$title}}></livewire:shared.title>
    <livewire:expenses.listing></livewire:expenses.listing>

    </div>

</x-app-layout>

