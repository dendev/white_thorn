import { defineConfig } from 'vite';
import laravel, { refreshPaths } from 'laravel-vite-plugin';

export default defineConfig({
    plugins: [
        laravel({
            input: [
                'resources/scss/guest.scss',
                'resources/js/guest.js',
            ],
            refresh: [
                ...refreshPaths
            ],
            buildDirectory: 'build/guest'
        }),
    ],
});

// https://github.com/pkfan/setup-laravel-vite-for-multiple-tailwind.config.js
