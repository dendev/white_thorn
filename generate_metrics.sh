#!/bin/bash

phpmetrics --report-html=docs/metrics ./app --junit='phpunit.xml'

if [ "$1" == '-o' ] || [ "$1" == '--open' ]
then
    xdg-open ./docs/metrics/index.html;
fi;

exit $?;
