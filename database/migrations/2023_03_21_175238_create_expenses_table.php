<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('expenses', function (Blueprint $table) {
            $table->id();
            $table->string('amount');
            $table->string('accounting_date');
            $table->foreignId('type_id')->constrained('expense_available_types');
            $table->foreignId('emitter_id')->constrained();
            $table->foreignId('receiver_id')->constrained();
            //$table->foreignId('status_id')->constrained('expense_statuses');
            $table->foreignId('status_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('expenses');
    }
};
