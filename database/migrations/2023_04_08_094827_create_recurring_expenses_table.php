<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('recurring_expenses', function (Blueprint $table) {
            $table->id();
            $table->foreignId('emitter_id')->constrained();
            $table->foreignId('receiver_id')->constrained();
            $table->foreignId('type_id')->constrained('expense_available_types');
            $table->float('fixed_amount');
            $table->string("frequency");
            $table->string("day")->nullable();
            $table->string("time")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('recurring_expenses');
    }
};
