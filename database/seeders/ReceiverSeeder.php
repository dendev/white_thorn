<?php

namespace Database\Seeders;

use App\Models\Receiver;
use App\Traits\UtilSeeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ReceiverSeeder extends Seeder
{
    use UtilSeeder;

    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $this->_reset_table('receivers');

        $datas = [
            //['name' => "", 'identity' => '', 'company_number' => '', 'email' => '',],
            ['name' => "Imact", 'identity' => 'syndic_imact', 'company_number' => '12', 'email' => 'test_imact@test.com' ],
        ];

        foreach( $datas as $data )
            \ReceiverManager::create($data);

        // use fake or not ( see .env )
        $this->_fake_seeder(5, 20, Receiver::class);
    }
}
