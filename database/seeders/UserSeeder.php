<?php
namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datas = [
            ['email' => env('CLI_EMAIL'), 'name' => env('CLI_NAME'), 'password' => env('CLI_PASSWORD')],
            ['email' => env('LK_EMAIL'), 'name' => env('LK_NAME'), 'password' => env('LK_PASSWORD')],
        ];

        foreach( $datas as $data )
        {
            // create user
            $user = User::firstOrCreate([
                'email' => $data['email'],
            ], [
                    'name' => $data['name'],
                    'email' => $data['email'],
                    'password' => Hash::make($data['password']),
                ]
            );

            // add roles
            //$user->assignRole('admin');
        }
    }
}
