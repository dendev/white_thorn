<?php

namespace Database\Seeders;

use App\Models\ExpenseAvailableType;
use App\Traits\UtilSeeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ExpenseAvailableTypeSeeder extends Seeder
{
    use UtilSeeder;

    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $this->_reset_table('expense_available_types');

        $datas = [
            //['label' => "", 'identity' => '', 'code' => '', 'description' => '',],
            ['label' => "Ménage des communes", 'identity' => 'commons_household', 'code' => 'CH01', 'decription' => 'test_imact@test.com' ],
        ];

        foreach( $datas as $data )
            \ExpenseManager::create_available_type($data);

        // use fake or not ( see .env )
        $this->_fake_seeder(5, 20, ExpenseAvailableType::class);
    }
}
