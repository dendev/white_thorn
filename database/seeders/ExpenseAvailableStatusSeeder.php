<?php

namespace Database\Seeders;

use App\Models\ExpenseAvailableStatus;
use App\Traits\UtilSeeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ExpenseAvailableStatusSeeder extends Seeder
{
    use UtilSeeder;

    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $this->_reset_table('expense_available_statuses');
        $datas = [
            //['label' => "", 'identity' => '', 'code' => '', 'icon' => '', 'color' => '', 'description' => '',],
            ['label' => "Note de frais crée et en attente d'envoi automatique",     'identity' => 'created',    'code' => 'A00', 'icon' => 'fa-solid fa-square-plus',       'color' => '', 'description' => "La note de frais est crée mais pas encore envoyée." ],
            ['label' => "Note de frais crée et envoyé par mail",                    'identity' => 'send',       'code' => 'A01', 'icon' => 'fa-regular fa-paper-plane-top', 'color' => '', 'description' => "La note de frais est crée et envoyée." ],
            ['label' => "En delais d'attente pour recevoir le paiement",            'identity' => 'waiting',    'code' => 'A05', 'icon' => 'fa-solid fa-hourglass',         'color' => '', 'description' => "Un delais d'attente est en cours afin que la note soit payé." ],
            ['label' => "Réception du paiement à vérifier",                         'identity' => 'checking',   'code' => 'B00', 'icon' => 'fa-solid fa-user-check',        'color' => '', 'description' => "Le délais d'attente pour le paiement est finie. Il faut vérifier sa reception." ],
            ['label' => "Paiement recut. La note de frais est terminée",            'identity' => 'paid',       'code' => 'B07', 'icon' => 'fa-solid fa-check',             'color' => '', 'description' => "Paiement recut donc fin de la note de frais." ],
            ['label' => "Paiement non recut. Une relance par email va être envoyé", 'identity' => 'unpaid',     'code' => 'B08', 'icon' => 'fa-solid fa-xmark',             'color' => '', 'description' => "La note de frais n'est toujours pas payé." ],
        ];

        foreach( $datas as $data )
            \ExpenseManager::create_available_status($data);

        // use fake or not ( see .env )
        //$this->_fake_seeder(5, 20, ExpenseAvailableStatus::class);
    }
}
