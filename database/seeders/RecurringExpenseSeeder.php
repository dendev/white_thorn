<?php

namespace Database\Seeders;

use App\Models\Receiver;
use App\Models\RecurringExpense;
use App\Traits\UtilSeeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class RecurringExpenseSeeder extends Seeder
{
    use UtilSeeder;

    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $this->_reset_table('recurring_expenses');

        $datas = [
            //['emitter' => "", 'receiver' => '', 'type' => '', 'fixed_amount' => '',, 'frequency' => '', 'day' => '', 'time' => ''],
            ['emitter' => "1", 'receiver' => '1', 'type' => '1', 'fixed_amount' => '60', 'frequency' => 'month', 'day' => '20', 'time' => '01:00'],
        ];

        foreach( $datas as $data )
            \RecurringExpenseManager::create($data);

        // use fake or not ( see .env )
        //$this->_fake_seeder(5, 20, RecurringExpense::class);
    }
}
