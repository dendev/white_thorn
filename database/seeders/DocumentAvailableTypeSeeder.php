<?php

namespace Database\Seeders;

use App\Models\DocumentAvailableType;
use App\Models\ExpenseAvailableType;
use App\Traits\UtilSeeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DocumentAvailableTypeSeeder extends Seeder
{
    use UtilSeeder;

    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $this->_reset_table('document_available_types');

        $datas = [
            //['label' => "", 'identity' => '', 'code' => '', 'template' => '', description' => '',],
            ['label' => "Note de frais", 'identity' => 'expense_report', 'code' => 'ER01', 'template' => 'expense_report.pdf',  'description' => 'Note de frais servant à demander un remboursement de la par de la coproprieté' ],
        ];

        foreach( $datas as $data )
            \DocumentManager::create_available_type($data);

        // use fake or not ( see .env )
        $this->_fake_seeder(5, 20, DocumentAvailableType::class);
    }
}
