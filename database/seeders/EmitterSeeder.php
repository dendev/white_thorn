<?php

namespace Database\Seeders;

use App\Traits\UtilSeeder;
use Illuminate\Database\Seeder;

class EmitterSeeder extends Seeder
{
    use UtilSeeder;

    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $this->_reset_table('emitters');

        $datas = [
            //['name' => "", 'identity' => '', 'bank_account' => '', 'email' => '', 'is_active' => '' ],
            ['name' => "Laurence Kinet", 'identity' => 'laurence_kinet', 'bank_account' => 'BE08096969000113', 'email' => 'lk@test.be', 'is_active' => true],
        ];

        // create
        foreach( $datas as $data )
            \EmitterManager::create($data);

        // use fake or not ( see .env )
        //$this->_fake_seeder(5, 20, EmitterModel::class);
    }
}
