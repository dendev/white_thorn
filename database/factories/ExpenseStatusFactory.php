<?php
namespace Database\Factories;

use App\Models\Expense;
use Illuminate\Database\Eloquent\Factories\Factory;

class ExpenseStatusFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        $expense = Expense::factory()->create();

        return [
            'expense_id' => $expense->id,
            'status_id' => $expense->status_id,
            'set_at' => $this->faker->dateTime,
        ];
    }
}
