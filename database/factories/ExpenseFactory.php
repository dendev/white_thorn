<?php
namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ExpenseFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        $type = ExpenseAvailableTypeFactory::new()->create();
        $emitter = EmitterFactory::new()->create();
        $receiver = ReceiverFactory::new()->create();
        //$status = ExpenseStatusFactory::new()->make();

        return [
            'amount' => $this->faker->randomFloat(2),
            'type_id' => $type->id,
            'emitter_id' => $emitter->id,
            'receiver_id' => $receiver->id,
            'accounting_date' => $this->faker->date(),
        ];
    }
}
