#!/bin/bash

phpunit --coverage-html docs/coverage

if [ "$1" == '-o' ] || [ "$1" == '--open' ]
then
    xdg-open ./docs/coverage/index.html;
fi;

exit $?;
