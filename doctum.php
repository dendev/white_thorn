<?php

use Doctum\Doctum;
use Symfony\Component\Finder\Finder;
use Doctum\RemoteRepository\GitHubRemoteRepository;

$iterator = Finder::create()
    ->files()
    ->name('*.php')
    ->exclude('resources')
    ->exclude('tests')
    ->in('./app');


return new Doctum($iterator, [
    'title' => 'White Thorn',
    'language' => 'fr', // Could be 'fr'
    'build_dir' => __DIR__ . '/docs/build',
    'cache_dir' => __DIR__ . '/docs/cache',
    'remote_repository' => new GitHubRemoteRepository('dendev/white_thorn', 'https://gitlab.com/dendev/white_thorn'),
    'default_opened_level' => 2 ]);
