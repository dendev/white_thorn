#!/bin/bash

php doctum.phar update doctum.php;

if [ "$1" == '-o' ] || [ "$1" == '--open' ]
then
    xdg-open ./docs/build/index.html;
fi;

exit $?;
