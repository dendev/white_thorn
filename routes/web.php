<?php

use App\Http\Controllers\ExpenseController;
use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return redirect()->route('pages.dashboard');
});

Route::get('/dashboard', [ExpenseController::class, 'list'])->name('pages.dashboard');

Route::get('/expense/create', [ExpenseController::class, 'create'])->name('pages.expense.create');
Route::get('/expense/{id}', [ExpenseController::class, 'show'])->name('pages.expense.show');

Route::get('/mailable', function () {

    $expense = \App\Models\Expense::find(1);
    return new App\Mail\ReceiverExpense(['expense' => $expense]);
});

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__.'/auth.php';
