<?php
return [
    'states' => [
        'create' => [
            'from' => [''],
            'to' => ['created']
        ],
        'created' => [
            'from'  => ['created'],
            'to' => ['send']
        ],
        'send' => [
            'from' => ['send', 'unpaid'],
            'to' => ['waiting'],
        ],
        'waiting'=> [
            'from' => ['waiting'],
            'to' => ['checking'],
        ],
        'checking'=> [
            'from' => ['checking'],
            'to' => ['paid', 'unpaid'],
        ],
        'paid'=> [
            'from' => ['paid'],
            'to' => [],
        ],
        'unpaid'=> [
            'from' => ['unpaid'],
            'to' => ['send'],
        ]
    ]
];
