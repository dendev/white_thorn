<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

class ExpenseStatus extends Model

{
    use HasFactory;

    protected $fillable = ['id', 'expense_id', 'status_id', 'set_at' ];
    protected $casts = [ 'set_at' => 'datetime' ];

    public function type(): Relation
    {
        return $this->hasOne(ExpenseAvailableStatus::class, 'id', "status_id");
    }

    public function expense(): Relation
    {
        return $this->hasOne(Expense::class);
    }
}
