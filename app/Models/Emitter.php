<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Emitter extends Model

{
    use HasFactory;

    protected $fillable = ['id', 'name', 'bank_account', 'email', 'is_active'];
}
