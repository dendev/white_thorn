<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Document extends Model
{
    use HasFactory;

    protected function url(): Attribute
    {
        return Attribute::make(
            get: fn ( $value, $attributes) => env('APP_URL') . Storage::url($attributes['path'])
        );
    }

    protected function path(): Attribute
    {
        return Attribute::make(
            get: fn ( $value, $attributes) => Storage::path($attributes['path'])
        );
    }
}
