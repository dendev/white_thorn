<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

class Expense extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'amount',
        'type_id',
        'emitter_id',
        'receiver_id',
        'accounting_date',
    ];

    protected $casts = [
        'accounting_date' => 'datetime'
    ];
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    *

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function add_status(ExpenseAvailableStatus $status)
    {
        $this->status_id = $status->id;
    }

    public function check_status_identity($identity)
    {
        return $this->get_status_identity() === $identity;
    }

    public function get_state()
    {
       return $this->get_status_identity();
    }

    public function get_status_identity()
    {
        return $this->status->type->identity;
    }

    public function get_mails()
    {
        $mail_histories = MailHistory::where('target_model', get_class($this))
            ->where('target_id', $this->id)
            ->get();

        return $mail_histories;
    }

    /*
    public function get_documents($identity = false)
    {
        $rq = ExpenseDocument::where('expense_id', $this->id);

        if( $identity )
            $rq = $rq->where('identity', $identity);

        $expense_documents = $rq->get();

        return $expense_documents;
    }
    */
    /*
    |--------------------------------------------------------------------------
    | RELATIONS https://laravel.com/docs/9.x/eloquent-relationships
    |--------------------------------------------------------------------------
    */
    public function type(): Relation
    {
        return $this->belongsTo(ExpenseAvailableType::class);
    }

    public function emitter(): Relation
    {
        return $this->belongsTo(Emitter::class);
    }

    public function receiver(): Relation
    {
        return $this->belongsTo(Receiver::class);
    }

    public function status(): Relation // current status != all
    {
        //return $this->hasOneThrough(ExpenseAvailableStatus::class, ExpenseStatus::class, 'status_id', 'id');
        return $this->hasOne(ExpenseStatus::class, 'id', 'status_id');
    }

    public function statuses(): Relation // all status as an historic
    {
        return $this->belongsToMany(ExpenseAvailableStatus::class, 'expense_statuses', 'expense_id', 'status_id')->withPivot('expense_id', 'status_id', 'created_at', 'updated_at');
    }

    public function documents(): Relation
    {
        return $this->hasManyThrough(Document::class, ExpenseDocument::class, 'expense_id', 'id', 'id', 'document_id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES https://laravel.com/docs/9.x/eloquent#query-scopes
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS https://laravel.com/docs/9.x/eloquent-mutators#defining-an-accessor
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS https://laravel.com/docs/9.x/eloquent-mutators#defining-a-mutator
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | EVENTS https://laravel.com/docs/9.x/events
    |--------------------------------------------------------------------------
    */
}
