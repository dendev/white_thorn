<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ExpenseAvailableType extends Model

{
    use HasFactory;

    protected $fillable = ['id', 'label', 'identity', 'code', 'description'];
}
