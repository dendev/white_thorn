<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ExpenseDocument extends Model
{
    use HasFactory;

    public function type()
    {
        return $this->hasOne(DocumentAvailableType::class, 'id', 'id');
    }
}
