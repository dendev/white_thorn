<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ExpenseController extends Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('auth');
        $this->middleware('verified');
    }


    public function list()
    {
        return view('pages/dashboard', ['title' => 'Notes de crédit']);
    }

    public function show($id)
    {
        $expense = \App\Models\Expense::find($id);

        return view('pages/expense/show', ['title' => __('expense.show_title'), 'expense' => $expense->id]);
    }

    public function create()
    {
        return view('pages/expense/create', ['title' => __('expense.create_title')]);
    }
}
