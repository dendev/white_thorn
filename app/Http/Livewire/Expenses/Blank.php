<?php

namespace App\Http\Livewire\Expenses;

use Livewire\Component;

class Blank extends Component
{
    public function render()
    {
        return view('livewire.expenses.blank');
    }
}
