<?php

namespace App\Http\Livewire\Expenses;

use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Listing extends Component
{
    public $expense_ids ;
    public $is_empty;

    public function mount()
    {
        $user = Auth::user();
        $this->expense_ids = \ExpenseManager::get_expenses_of_user($user)->pluck('id');
        $this->is_empty = $this->expense_ids->isEmpty();
    }

    public function render()
    {
        return view('livewire.expenses.listing');
    }
}
