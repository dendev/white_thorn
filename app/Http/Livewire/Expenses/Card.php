<?php

namespace App\Http\Livewire\Expenses;

use App\Models\Expense;
use Illuminate\Support\Facades\Auth;
use Jantinnerezo\LivewireAlert\LivewireAlert;
use Livewire\Component;

class Card extends Component
{
    use LivewireAlert;

    public $expense;
    public array $cans;

    public bool $is_done;

    public function mount(Expense $expense)
    {
        $this->expense = $expense;

        $this->cans = [
            'show' => true,
            'verify' => false,
        ];

        $this->is_done = false;
    }

    public function render()
    {
        return view('livewire.expenses.card');
    }

    public function handle_click_paid()
    {
        $ok = \WorkflowManager::checkingToPaid(Auth::user(), $this->expense );

        if( $ok )
            $this->alert('success', __('expense.checking_to_paid_success'));
        else
            $this->alert('error', __('expense.checking_to_paid_error'));

        $this->is_done = true;
    }

    public function handle_click_unpaid()
    {
        $ok = \WorkflowManager::checkingToUnpaidToSend(Auth::user(), $this->expense );

        if( $ok )
            $this->alert('success', __('expense.checking_to_unpaid_success'));
        else
            $this->alert('error', __('expense.checking_to_unpaid_error'));

        $this->is_done = true;
    }
}
