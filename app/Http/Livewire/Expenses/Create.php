<?php

namespace App\Http\Livewire\Expenses;

use App\Models\Emitter;
use App\Models\ExpenseAvailableStatus;
use App\Models\ExpenseAvailableType;
use App\Models\Receiver;
use Illuminate\Support\Facades\Auth;
use Jantinnerezo\LivewireAlert\LivewireAlert;
use Livewire\Component;

class Create extends Component
{
    use LivewireAlert;


    public array $type_options;
    public array $emitter_options;
    public array $receiver_options;

    public float $amount;
    public string $accounting_date;
    public string $type_id;
    public string $emitter_id;
    public string $receiver_id;
    public string $remark;

    protected $rules = [
        'amount' => 'required|between:0,9999999999.00',
        'accounting_date' => 'required',
        'type_id' => 'required',
        'emitter_id' => 'required',
        'receiver_id' => 'required',
        'remark' => '',
    ];

    public function mount()
    {
        $this->_init_select_options();
    }

    public function render()
    {
        return view('livewire.expenses.create');
    }

    public function submit()
    {
        $this->validate();

        $user = Auth::user();

        $args = [
            'amount' => $this->amount,
            'accounting_date' => $this->accounting_date,
            'type' => ExpenseAvailableType::find($this->type_id),
            'emitter' => Emitter::find($this->emitter_id),
            'receiver' => Receiver::find($this->receiver_id),
        ];

        $expense = \WorkflowManager::toCreate($user, $args);
        if( $expense )
            $this->alert('success', 'Success is approaching!');
        else
            $this->alert('warning', 'Success is approaching!');

        return redirect()->to('dashboard');
    }

    private function _init_select_options()
    {
        $type_options = ExpenseAvailableType::all()->sortBy('label');
        $emitter_options = Emitter::all()->sortBy('label');
        $receiver_options = Receiver::all()->sortBy('label');

        $this->type_options = $type_options->pluck('label', 'id')->toArray();
        $this->emitter_options = $emitter_options->pluck('name', 'id')->toArray();
        $this->receiver_options = $receiver_options->pluck('name', 'id')->toArray();

        $this->type_id = $type_options->first()->id;
        $this->emitter_id = $emitter_options->first()->id;
        $this->receiver_id = $receiver_options->first()->id;
    }
}
