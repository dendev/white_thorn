<?php

namespace App\Http\Livewire\Shared;

use Livewire\Component;

class Title extends Component
{
    public string $title;

    public function render()
    {
        return view('livewire.shared.title');
    }
}
