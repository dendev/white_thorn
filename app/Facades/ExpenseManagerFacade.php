<?php
namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class ExpenseManagerFacade extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return 'expense_manager';
    }
}
