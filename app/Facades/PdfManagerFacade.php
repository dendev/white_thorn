<?php
namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class PdfManagerFacade extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return 'pdf_manager';
    }
}
