<?php
namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class MailManagerFacade extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return 'mail_manager';
    }
}
