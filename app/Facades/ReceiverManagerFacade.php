<?php
namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class ReceiverManagerFacade extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return 'receiver_manager';
    }
}
