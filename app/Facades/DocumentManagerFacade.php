<?php
namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class DocumentManagerFacade extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return 'document_manager';
    }
}
