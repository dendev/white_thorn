<?php
namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class EmitterManagerFacade extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return 'emitter_manager';
    }
}
