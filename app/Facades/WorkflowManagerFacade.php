<?php
namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class WorkflowManagerFacade extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return 'workflow_manager';
    }
}
