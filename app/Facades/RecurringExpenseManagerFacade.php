<?php
namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class RecurringExpenseManagerFacade extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return 'recurring_expense_manager';
    }
}
