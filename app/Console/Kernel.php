<?php

namespace App\Console;

use App\Console\Commands\ExpenseChecking;
use App\Console\Commands\ExpenseCreate;
use App\Console\Commands\ExpenseSendAndWaiting;
use App\Models\RecurringExpense;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Log;

class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     */
    protected function schedule(Schedule $schedule): void
    {
        $this->_run_create_recurring_expenses($schedule);
        $this->_run_expense_workflow($schedule);
    }

    /**
     * Register the commands for the application.
     */
    protected function commands(): void
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }

    private function _run_create_recurring_expenses(Schedule $schedule): int
    {
        Log::stack(['cli'])->info('[Console::Kernel:_run_create_recurring_expense] CKrcre01: Automatic start run create recurring expense cmd');


        $recurrings = $this->_get_recurring_expenses();

        $nb_total = $recurrings->count();
        $nb_created = 0;
        foreach( $recurrings as $recurring )
        {
            $frequency = $recurring->frequency;
            $day = $recurring->day;
            $time = $recurring->time;

            if( $frequency )
            {
                $args = [
                    $recurring['emitter_id'],
                    $recurring['receiver_id'],
                    $recurring['fixed_amount'],
                    $recurring['type_id'],
                ];

                if ($frequency === 'month')
                {
                    $schedule->command(ExpenseCreate::class, $args)->monthlyOn($day, $time);
                }
                else if ($frequency === 'day')
                {
                    $schedule->command(ExpenseCreate::class, $args)->dailyAt($time);
                }

                $nb_created++;
            }
        }

        $log_level = $nb_created === $nb_total ? 'info' : 'error';
        Log::stack(['cli'])->$log_level('[Console::Kernel:_run_create_recurring_expense] CKrcre01: End automatic start run create recurring expense cmd', [
            'nb_total' => $nb_total,
            'nb_created' => $nb_created
        ]);

        return $nb_created;
    }

    private function _run_expense_workflow($schedule)
    {
        Log::stack(['cli'])->info('[Console::Kernel:_run_expense_workflow] CKrew01: Automatic start run expense cmd');

        $is_success_send_and_waiting = $schedule->command(ExpenseSendAndWaiting::class)->dailyAt('01:05');
        $is_success_checking = $schedule->command(ExpenseChecking::class)->dailyAt('01:10');
        $is_success_set_perms = $schedule->exec('./set_perms.sh')->dailyAt('01:15');

        Log::stack(['cli'])->info('[Console::Kernel:_run_expense_workflow] CKrew02: End automatic run expense cmd', [
            'is_success_send_and_waiting' => $is_success_send_and_waiting,
            'is_success_checking' => $is_success_checking,
            'is_success_set_perms' => $is_success_set_perms
        ]);
    }

    private function _get_recurring_expenses(): Collection
    {
        return RecurringExpense::all();
    }
}
