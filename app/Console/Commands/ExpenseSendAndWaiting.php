<?php

namespace App\Console\Commands;

use App\Models\Emitter;
use App\Models\Expense;
use App\Models\ExpenseAvailableStatus;
use App\Models\ExpenseAvailableType;
use App\Models\Receiver;
use App\Traits\UtilCommand;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Symfony\Component\Console\Command\Command as CommandAlias;

class ExpenseSendAndWaiting extends Command
{
    use UtilCommand;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'expense:send_and_waiting {expense_id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send expense to receiver and set waiting mode';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->log_start();

        $expenses = $this->_get_expenses('created');

        $user = $this->_get_cli_user();

        $nb_total = $expenses->count();
        $nb_done = 0;
        //$nb_future = 0;
        foreach( $expenses as $expense )
        {
            $is_send = \WorkflowManager::createdToSend($user, $expense);
            $expense->refresh();
            $is_waiting = \WorkflowManager::sendToWaiting($user, $expense);

            if( $is_send && $is_waiting )
                $nb_done++;
        }

        // success
        $is_success = $nb_total == ( $nb_done ) ? CommandAlias::SUCCESS : CommandAlias::FAILURE; // bof

        // log
        $this->log_end(!(bool)$is_success, null, [
            'nb_total' => $nb_total,
            'nb_done' => $nb_done,
            //'nb_future' => $nb_future,
        ]);

        // end
        return $is_success;
    }

    // -
}
