<?php

namespace App\Console\Commands;

use App\Models\Emitter;
use App\Models\Expense;
use App\Models\ExpenseAvailableType;
use App\Models\Receiver;
use App\Traits\UtilCommand;
use Illuminate\Console\Command;

class ExpenseChecking extends Command
{
    use UtilCommand;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'expense:checking {expense_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Open checking period';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->log_start();

        $expenses = $this->_get_expenses('waiting');

        $user = $this->_get_cli_user();

        foreach( $expenses as $expense )
        {
            \WorkflowManager::waitingToChecking($user, $expense);
        }

        return true;
    }

}
