<?php

namespace App\Console\Commands;

use App\Models\Emitter;
use App\Models\ExpenseAvailableType;
use App\Models\Receiver;
use App\Models\User;
use App\Traits\UtilCommand;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Symfony\Component\Console\Command\Command as CommandAlias;

class ExpenseCreate extends Command
{
    use UtilCommand;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'expense:create {emitter_id} {receiver_id} {amount} {type_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create basic expend';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        // log
        $this->log_start();

        // check
        $type = ExpenseAvailableType::find( $this->argument('type_id'));
        $emitter = \App\Models\Emitter::find($this->argument('emitter_id'));
        $receiver = Receiver::find( $this->argument('receiver_id'));

        // pack
        $args = [
            'amount' => $this->argument('amount'),
            'type' => $type,
            'emitter' => $emitter,
            'receiver' => $receiver,
            'accounting_date' => now(), // change to default value // use accounting_variation from db 0 == now, -1 == -1day||month
        ];

        $user = $this->_get_cli_user();

        // do it
        $expense = \WorkflowManager::toCreate($user, $args);

        // success
        $is_success = $expense ? CommandAlias::SUCCESS : CommandAlias::FAILURE;

        // log
        $this->log_end(!(bool)$is_success);

        // end
        return $is_success;
    }
}
