<?php

namespace App\Traits;

use Illuminate\Support\Facades\DB;

trait UtilSeeder
{
    protected function _reset_table(string $table_name): void
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table($table_name)->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

    /**
     * @throws Exception
     */
    protected function _fake_seeder(int $min, int $max, string $model_classname): void
    {
        if( env('FAKE_SEEDER_IS_ENABLED', false) )
        {
            $nb = random_int($min, $max);
            $model_classname::factory()->count($nb)->create();
        }
    }
}
