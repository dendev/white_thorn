<?php

namespace App\Traits;

use App\Models\Expense;
use App\Models\ExpenseAvailableStatus;
use App\Models\User;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;

trait UtilCommand
{
    public function log_start(?string $label = null): void
    {
        if( is_null( $label ))
        {
            $tmp = explode('\\', get_class($this));
            $classname = end($tmp );
            $label = "[$classname] " . strtok($this->signature, ' ') . ': Start';
        }

        Log::stack(['cli'])->info($label, [
            'arguments' => implode(',', $this->arguments()),
            'options' => implode(',', $this->options())
        ]);
    }

    public function log_end(bool $is_success, ?string $label = null, ?array $context = null ): void
    {
        if( is_null( $label ))
        {
            $tmp = explode('\\', get_class($this));
            $classname = end($tmp );
            $label = "[$classname] " . strtok($this->signature, ' ') . ': End';
        }

        $log_level = $is_success ? 'info' : 'error';

        $context = is_null($context) ? [] : $context;
        $context['is_success'] = $is_success;

        Log::stack(['cli'])->$log_level($label, $context);
    }

    private function _get_cli_user()
    {
        $user = User::where('email', env('CLI_EMAIL'))->first();
        return $user;
    }

    private function _get_expenses(string $type_identity): Collection
    {
        $ref_status = ExpenseAvailableStatus::where('identity', $type_identity)->first();

        $id = $this->argument('expense_id');
        if( $id )
        {
            $expense = Expense::find($this->argument('expense_id'));

            $expenses= collect();
            if( $expense->status->status_id === $ref_status->id )
                $expenses->push($expense);
        }
        else
        {
            $expenses = Expense::whereHas('status', function ($query) use ( $ref_status) {
                return $query->where('status_id', $ref_status->id);
            })->get();
        }

        return $expenses;
    }
}
