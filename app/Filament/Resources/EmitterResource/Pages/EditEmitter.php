<?php

namespace App\Filament\Resources\EmitterResource\Pages;

use App\Filament\Resources\EmitterResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\EditRecord;

class EditEmitter extends EditRecord
{
    protected static string $resource = EmitterResource::class;

    protected function getActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
