<?php

namespace App\Filament\Resources\EmitterResource\Pages;

use App\Filament\Resources\EmitterResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateEmitter extends CreateRecord
{
    protected static string $resource = EmitterResource::class;
}
