<?php

namespace App\Filament\Resources\EmitterResource\Pages;

use App\Filament\Resources\EmitterResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\ListRecords;

class ListEmitters extends ListRecords
{
    protected static string $resource = EmitterResource::class;

    protected function getActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }
}
