<?php
namespace App\Services;


use App\Models\Expense;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class WorkflowManagerService
{
    public function toCreate(User $user, array $args): ?Expense
    {
        $transition = 'toCreated';

        $can = $this->_check_can($user, $transition, null);
        if( $can )
            return \ExpenseManager::create($args);

        return null;
    }

    public function createdToSend(User $user, Expense $expense, array $args = [])
    {
        $transition = 'createdToSend';

        $can = $this->_check_can($user, $transition, $expense );
        if( $can )
            return \ExpenseManager::send($expense);

        return false;
    }

    public function sendToWaiting(User $user, Expense $expense, array $args = [])
    {
        $transition = 'sendToWaiting';

        $can = $this->_check_can($user, $transition, $expense );
        if( $can )
            return \ExpenseManager::waiting($expense);

        return false;
    }

    public function waitingToChecking(User $user, Expense $expense, array $args = [])
    {
        $transition = 'waitingToChecking';

        $can = $this->_check_can($user, $transition, $expense );
        if( $can )
            return \ExpenseManager::checking($expense);

        return false;
    }

    public function checkingToPaid(User $user, Expense $expense, array $args = []): bool // end
    {
        $transition = 'checkingToPaid';

        $can = $this->_check_can($user, $transition, $expense );
        if( $can )
            return \ExpenseManager::paid($args);

        return false;
    }

    public function checkingToUnpaid(User $user, Expense $expense, array $args = []): bool
    {
        $transition = 'checkingToUnpaid';

        $can = $this->_check_can($user, $transition, $expense );
        if( $can )
            return \ExpenseManager::unpaid($expense);

        return false;
    }

    public function unpaidToSend(User $user, Expense $expense, array $args = []) // re run
    {
        $transition = 'unpaidToSend';

        $can = $this->_check_can($user, $transition, $expense );
        if( $can )
            return \ExpenseManager::resend($expense);

        return false;
    }

    public function checkingToUnpaidToSend(User $user, Expense $expense, array $args = []): bool
    {
        $is_unpaid = $this->checkingToUnpaid($user, $expense, $args);
        $expense->refresh();
        $is_send = $this->unpaidToSend($user, $expense, $args);

        return $is_send && $is_unpaid;
    }

    public function test_me()
    {
        return 'workflow_manager';
    }

    public function get_availables_transitions(?Expense $expense = null): array
    {
        $states = $this->_define();

        if( $expense )
        {
            $state = $expense->get_state();
            $transitions = array_key_exists($state, $states) ? $states[$state] : [];
        }
        else
        {
            $first_key =  array_key_first($states);
            $transitions = $states[$first_key];
        }


        return $transitions;
    }

    // -
    private function _check_can(User $user, string $transition, ?Expense $expense = null)
    {
        $user_can = $this->_user_can($user, $transition);
        $workflow_can = $this->_workflow_can($transition, $expense);

        return $user_can && $workflow_can;
    }

    private function _user_can(User $user, string $transition): bool
    {
        $can = $user->can($transition, Expense::class);

        if( ! $can )
            \Log::stack(['workflow'])->error("[WorkflowManagerService::_user_can] WMSuc01: User can't run this transition: $transition", [
                'user_id' => $user->id,
                'user_name' => $user->name,
                'can' => $can,
                'transition' => $transition,
            ]);

        return $can;
    }

    private function _workflow_can(string $transition, ?Expense $expense = null ): bool
    {
        $can = false;

        $transitions = $this->get_availables_transitions($expense);
        if( array_key_exists('to', $transitions) && array_key_exists('from', $transitions) )
        {
            foreach( $transitions['from'] as $from)
            {
                foreach( $transitions['to'] as $to )
                {
                    $available = $from . 'to' . $to;
                    if( $available === strtolower($transition))
                    {
                        $can = true;
                        break;
                    }
                }
            }
        }

        if( ! $can )
            \Log::stack(['workflow'])->error("[WorkflowManagerService::_workflow_can] WMSwc01: Workflow can't run this transition: $transition", [
                'expense' => $expense->id,
                'can' => $can,
                'state' => $expense->get_state(),
                'transition' => $transition,
                'transitions' => json_encode($transitions, true)
            ]);

        return $can;
    }

    private function _define()
    {
        return Config('white_thorn.workflow.states');
    }
}
