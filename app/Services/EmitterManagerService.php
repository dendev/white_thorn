<?php
namespace App\Services;


use App\Models\Emitter;
use Illuminate\Database\Eloquent\Model;

class EmitterManagerService implements ICreateManager
{
    public function create(array $values): Model|null
    {
        $emitter = null;

        // check
        $name = array_key_exists('name', $values) ? $values['name'] : false;
        $bank_account = array_key_exists('bank_account', $values) ? $values['bank_account'] : false;
        $email = array_key_exists('email', $values) ? $values['email'] : false;
        $is_active = array_key_exists('is_active', $values) ? $values['is_active'] : false;

        // TODO if already exist ?
        if( $name && $bank_account )
        {
            $emitter = new Emitter([
                'name' => $name,
                'bank_account' => $bank_account,
                'email' => $email,
                'is_active' => $is_active
            ]);
            $emitter->save();
        }
        else
        {
            //\LogManager::save();
            $entity = null;
        }

        // end
        return $emitter;
    }

    public function test_me()
    {
        return 'emitter_manager';
    }
}
