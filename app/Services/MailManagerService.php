<?php
namespace App\Services;

use App\Mail\EmitterCheckingOpen;
use App\Mail\ReceiverExpense;
use App\Models\MailHistory;
use App\Traits\UtilService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class MailManagerService
{
    use UtilService;

    public function send(string $identity, array $args): bool
    {
        $is_send = false;

        //
        if( $identity === 'expense_report' )
            $is_send = $this->send_expense_report($identity, $args);
        else if( $identity === 'emitter_checking_open' )
            $is_send = $this->send_emitter_checking_open($identity, $args);

        // log
        $log_level = $is_send ? 'notice' : 'error';
        Log::stack(['mail'])->$log_level("MailManagerService:send] EMSs01: Mail $identity", [
            'email_identity' => $identity,
            'email_args' => implode(',', $args),
            'is_send' => $is_send,
        ]);

        // end
        return (bool)$is_send;
    }

    private function send_expense_report(string $identity, array $args): bool
    {
        $is_send = false;

        $expense = array_key_exists('expense', $args) ? $args['expense'] : false;
        $pdf = array_key_exists('pdf', $args) ? $args['pdf'] : false;
        if( $expense )
        {
            $to = $expense->receiver->email;
            $mailable = new ReceiverExpense([
                'expense' => $expense,
                'pdf' => $pdf,
            ]);

            Mail::to($to)
                ->send($mailable);

            $subject = $mailable->subject;

            $content = $mailable->content;

            $this->_save_history($identity, $subject, $to, $content, $expense->id, $expense::class);

            $is_send = true;
        }
        else
        {
            Log::stack(['mail'])->error("MailManagerService:_send_expense] EMSse01: Unable to instantiate expense model", [
                'email_identity' => $identity,
                'email_args' => implode(',', $args),
            ]);
        }

        return $is_send;
    }

    private function send_emitter_checking_open(string $identity, array $args): bool
    {
        $is_send = false;

        $expense = array_key_exists('expense', $args) ? $args['expense'] : false;
        if( $expense )
        {
            $to = $expense->emitter->email;
            $mailable = new EmitterCheckingOpen([
                'expense' => $expense,
            ]);

            Mail::to($to)
                ->send($mailable);

            $subject = $mailable->subject;

            $content = $mailable->content;

            $this->_save_history($identity, $subject, $to, $content, $expense->id, $expense::class);

            $is_send = true;
        }
        else
        {
            Log::stack(['mail'])->error("MailManagerService:_send_emitter_checking_open] EMSseco01: Unable to instantiate expense model", [
                'email_identity' => $identity,
                'email_args' => implode(',', $args),
            ]);
        }

        return $is_send;
    }

    public function test_me()
    {
        return 'mail_manager';
    }

    // -
    private function _save_history(string $identity, string $subject, string $to, string $content, int|string $target_id, string $target_model): bool
    {
        $is_saved = false;

        $already_exist = MailHistory::where('identity', $identity)
            ->where('to', $to)
            ->first();

        if( $already_exist )
        {
            $already_exist->nb_sent = $already_exist->nb_sent + 1;
            $already_exist->save();
        }
        else
        {
            $mail_history = new MailHistory();
            $mail_history->identity = $identity;
            $mail_history->subject = $subject;
            $mail_history->to = $to;
            $mail_history->content = $content;
            $mail_history->target_id = $target_id;
            $mail_history->target_model = $target_model;
            $mail_history->nb_sent = 1;

            $mail_history->save();
            $is_saved = true;
        }

        return $is_saved;
    }
}
