<?php
namespace App\Services;

use Illuminate\Database\Eloquent\Model;

use App\Models\Receiver;


class ReceiverManagerService implements ICreateManager
{
    public function create(array $values, bool $return_model = false):Model|null
    {
        $receiver = null;

        // check
        $name = array_key_exists('name', $values) ? $values['name'] : false;
        $company_number = array_key_exists('company_number', $values) ? $values['company_number'] : false;
        $email = array_key_exists('email', $values) ? $values['email'] : false;

        if( $name && $company_number && $email )
        {
            $receiver = new Receiver([
                'name' => $name,
                'company_number' => $company_number,
                'email' => $email,
            ]);
            $receiver->save();
        }
        else
        {
            //\LogManager::save();
            $entity = null;
        }

        return $receiver;
    }

    public function test_me()
    {
        return 'receiver_manager';
    }
}
