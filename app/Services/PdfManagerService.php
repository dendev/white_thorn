<?php
namespace App\Services;

use App\Traits\UtilService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use setasign\Fpdi\Fpdi;

class PdfManagerService
{
    use UtilService;

    public function create(string $type_identity, array $args = []): false|string
    {
        $pdf = false;

        if( $type_identity === 'expense_report' )
            $pdf = $this->_create_expense_report($args);

        return $pdf;
    }

    public function test_me()
    {
        return 'pdf_manager';
    }

    // -
    private function _create_expense_report(array $args): false|string
    {
        $path = false;

        $expense = array_key_exists('expense', $args) ? $args['expense'] : false;
        if( $expense )
        {
            $document_type = \DocumentManager::get_type('expense_report');

            if( $document_type)
            {
                $template_path = $document_type->template;

                if( $template_path)
                {
                    $full_path_template = storage_path('app/public/templates/' . $template_path);

                    // get template
                    $pdf = new Fpdi();
                    $pdf->AddPage();
                    $pdf->setSourceFile($full_path_template);
                    $tplId = $pdf->importPage(1);
                    $pdf->useTemplate($tplId);

                    // set datas in pdf
                    $pdf->SetAutoPageBreak(false);
                    $pdf->SetY(12);
                    $pdf->SetFont('Arial','B',12);
                    $pdf->Cell(182, 264, $expense->accounting_date->format('d/m/Y'), 0, 0, 'C');
                    $pdf->SetX(12);
                    $pdf->Cell(185, 321, $expense->amount  . ' euros', 0, 0, 'C');

                    // save
                    $file_name = Str::slug($expense->emitter->name . '_' . $expense->type->identity . '_' . $expense->updated_at) . '.pdf';
                    $save_path = storage_path("/app/public/expenses/$file_name");
                    $pdf->Output($save_path, "F");

                    $path = $save_path;

                    if( $save_path )
                    {
                        $document = \DocumentManager::create(['filename' => $file_name, 'extension' => '.pdf', 'path' => $path]);
                        \DocumentManager::add_to_expense($document, $expense);
                    }
                }
                else
                {
                    dd('LOG ERROR'); // TODO
                }
            }
        }

        return $path;
    }
}
