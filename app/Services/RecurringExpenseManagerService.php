<?php
namespace App\Services;


use App\Models\Emitter;
use App\Models\ExpenseAvailableType;
use App\Models\Receiver;
use App\Models\RecurringExpense;
use App\Traits\UtilService;
use Illuminate\Database\Eloquent\Model;

class RecurringExpenseManagerService implements ICreateManager
{
    use UtilService;

    public function create(array $values): Model|null
    {
        $recurring_expense = null;

        // get value
        $emitter = array_key_exists('emitter', $values) ? $values['emitter'] : false;
        $receiver = array_key_exists('receiver', $values) ? $values['receiver'] : false;
        $type = array_key_exists('type', $values) ? $values['type'] : false;
        $fixed_amount = array_key_exists('fixed_amount', $values) ? $values['fixed_amount'] : false;
        $frequency = array_key_exists('frequency', $values) ? $values['frequency'] : false;
        $day = array_key_exists('day', $values) ? $values['day'] : false;
        $time = array_key_exists('time', $values) ? $values['time'] : false;

        // check value
        $emitter = $this->_instantiate_if_id($emitter, Emitter::class);
        $receiver = $this->_instantiate_if_id($receiver, Receiver::class);
        $type = $this->_instantiate_if_id($type, ExpenseAvailableType::class);
        $fixed_amount = is_numeric($fixed_amount) ? $fixed_amount : false;
        $frequency = in_array($frequency, ['month', 'day', 'year'])? $frequency : 'month';
        $day = ( $day > 0 && $day < 32 ) ? $day : false;
        $time = (strtotime($time) === false || date('H:i', strtotime($time)) !== $time) ? '01:00' : $time;

        if( $emitter && $receiver && $type && $fixed_amount && $frequency  )
        {
            $recurring_expense = new RecurringExpense([
                'emitter_id' => $emitter->id,
                'receiver_id' => $receiver->id,
                'type_id' => $type->id,
                'fixed_amount' => $fixed_amount,
                'frequency' => $frequency,
                'day' => $day,
                'time' => $time
            ]);
            $recurring_expense->save();
        }
        else
        {
            dump( $emitter);
            dump( $receiver);
            dump( $type);
            dump( $fixed_amount);
            dump( $frequency);
            dd('TODO');
            //\LogManager::save();
        }

        // end
        return $recurring_expense;
    }

    public function test_me()
    {
        return 'recurring_expense_manager';
    }
}
