<?php
namespace App\Services;



use App\Models\Expense;
use App\Models\ExpenseAvailableStatus;
use App\Models\ExpenseAvailableType;
use App\Models\ExpenseStatus;
use App\Models\User;
use App\Traits\UtilService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;

class ExpenseManagerService implements ICreateManager
{
    use UtilService;

    public function add_status(string $identity, Expense $expense): bool
    {
        $expense_status = false;

        $status = ExpenseAvailableStatus::where('identity', $identity)->first();

        if ($status)
        {
            $expense_status = new ExpenseStatus([
                'expense_id' => $expense->id,
                'status_id' => $status->id,
            ]);
            $expense_status->save();

            $expense->status_id = $expense_status->id;
            $expense->save();
        }
        else
        {
            dd('TODO');
            // TODO
        }

        return $expense_status ? true : false;
    }

    public function checking($id_or_model_expense)
    {
        $is_open = false;

        $expense = $this->_instantiate_if_id($id_or_model_expense, Expense::class);

        if( $expense )
        {
            // define send day
            $waiting_at = $expense->status->updated_at;

            $waiting_delay = 5; // days

            $waiting_day = $waiting_at->addDays($waiting_delay);

            // can run it today
            //$test = Carbon::createFromFormat('m/d/Y H:i:s', '04/30/2023 10:20:00');
            //if( $waiting_day->format('Y-m-d') === $test->format('Y-m-d'))
            if( $waiting_day->format('Y-m-d') === now()->format('Y-m-d'))
            {
                // make && send mail
                $is_send = \MailManager::send('emitter_checking_open', [
                    'expense' => $expense,
                ]);

                // add new status
                $this->add_status('checking', $expense);
            }
            else
            {
                $waiting_day_string = $waiting_day->format('Y-m-d');
                Log::stack(['stack'])->info("[ExpenseManagerService:checking] EMSc01: Will be executed $waiting_day_string", [
                    'expense_id' => $expense->id,
                    'end_waiting_day' => $waiting_day_string,
                    'waiting_day' => $expense->status->updated_at->format('Y-m-d'),
                    'now' => now()->format('Y-m-d')
                ]);
            }
        }
        else
        {
            Log::stack(['stack'])->error("[ExpenseManagerService:checking] EMSc01: Can't instantiate model expense", [
                'expense_id' => $id_or_model_expense,
            ]);
        }


        return $is_open;
    }

    public function create(array $values): Model|null
    {
        $expense = null;

        // check
        $amount = array_key_exists('amount', $values) ? $values['amount'] : false;
        $type = array_key_exists('type', $values) ? $values['type'] : false;
        $emitter = array_key_exists('emitter', $values) ? $values['emitter'] : false;
        $receiver = array_key_exists('receiver', $values) ? $values['receiver'] : false;
        $accounting_date = array_key_exists('accounting_date', $values) ? $values['accounting_date'] : false;

        if ($amount && $type && $emitter && $receiver && $accounting_date)
        {
            // expense
            $expense = new Expense([
                'amount' => $amount,
                'type_id' => $type->id,
                'emitter_id' => $emitter->id,
                'receiver_id' => $receiver->id,
                'accounting_date' => $accounting_date
            ]);

            $expense->save();

            // status
            $this->add_status('created', $expense);
        }
        else
        {
            Log::stack(['stack'])->error('[ExpenseManagerService:create] EMSc01: unable to create expense', [
                'amount' => $amount,
                'type_id' => $type ? $type->id : 'not found',
                'emitter_id' => $emitter ? $emitter->id : 'not found',
                'receiver_id' => $receiver ? $receiver->id : 'not found',
                'accounting_date' => $accounting_date,
            ]);
        }

        return $expense;
    }

    public function create_available_type(array $values): Model|null
    {
        $type = null;

        // check
        $label = array_key_exists('label', $values) ? $values['label'] : false;
        $code = array_key_exists('code', $values) ? $values['code'] : false;

        $identity = array_key_exists('identity', $values) ? $values['identity'] : null;
        $description = array_key_exists('description', $values) ? $values['description'] : null;

        if ($label && $code) {
            $type = new ExpenseAvailableType([
                'label' => $label,
                'identity' => $identity,
                'code' => $code,
                'description' => $description,
            ]);
            $type->save();
        } else {
            // TODO log
            dd('rERR');
        }

        return $type;
    }

    public function create_available_status(array $values): Model|null
    {
        $status = null;

        // check
        $label = array_key_exists('label', $values) ? $values['label'] : false;
        $code = array_key_exists('code', $values) ? $values['code'] : false;

        $identity = array_key_exists('identity', $values) ? $values['identity'] : null;
        $description = array_key_exists('description', $values) ? $values['description'] : null;
        $icon = array_key_exists('icon', $values) ? $values['icon'] : null;
        $color = array_key_exists('color', $values) ? $values['color'] : null;

        if ($label && $code)
        {
            // todo already exist ?

            $type = new ExpenseAvailableStatus([
                'label' => $label,
                'identity' => $identity,
                'code' => $code,
                'icon' => $icon,
                'color' => $color,
                'description' => $description,
            ]);
            $type->save();
        }
        else
        {
            // TODO log
            dd('rERR');
        }

        return $status;
    }

    public function get_expenses_of_user(User $user)
    {
        return Expense::all();
    }

    public function paid($id_or_model_expense): bool
    {
        $is_done = false;

        $expense = $this->_instantiate_if_id($id_or_model_expense, Expense::class);

        if( $expense )
        {
            $this->add_status('paid', $expense);
            $is_done = true;
        }
        else
        {
            Log::stack(['stack'])->error("[ExpenseManagerService:paid] EMSw01: Can't instantiate model expense", [
                'expense_id' => $id_or_model_expense,
            ]);
        }


        return $is_done;
    }

    public function resend($id_or_model_expense)
    {
        $is_send = false;

        $expense = $this->_instantiate_if_id($id_or_model_expense, Expense::class);

        if( $expense )
        {
                // get pdf
                $pdf_path = $expense->documents->last()->path;

                // make && send mail
                $is_send = \MailManager::send('expense_report', [
                    'expense' => $expense,
                    'pdf' => $pdf_path,
                ]);

                // add new status
                $this->add_status('send', $expense);
        }
        else
        {
            Log::stack(['stack'])->error("[ExpenseManagerService:send] EMSs01: Can't instantiate model expense", [
                'expense_id' => $id_or_model_expense,
            ]);
        }


        return $is_send;
    }

    public function send($id_or_model_expense)
    {
        $is_send = false;

        $expense = $this->_instantiate_if_id($id_or_model_expense, Expense::class);

        if( $expense )
        {
            // define send date
            $send_date = now();
            $send_date->setDay(Config::get('white_thorn.general.send_day'));

            // can run it today
            //$test = Carbon::createFromFormat('m/d/Y H:i:s', '04/30/2023 10:20:00');
            //if( $send_day->format('Y-m-d') === $test->format('Y-m-d'))
            if( $send_date->format('Y-m-d') === now()->format('Y-m-d'))
            {
                // make pdf
                $pdf_path = \PdfManager::create('expense_report', [
                    'expense' => $expense
                ]);

                // make && send mail
                $is_send = \MailManager::send('expense_report', [
                    'expense' => $expense,
                    'pdf' => $pdf_path,
                ]);

                // add new status
                $this->add_status('send', $expense);
            }
            else
            {
                $send_day_string = $send_day->format('Y-m-d');
                Log::stack(['stack'])->info("[ExpenseManagerService:send] EMSs01: Will be executed $send_day_string", [
                    'expense_id' => $expense->id,
                    'send_day' => $send_day_string,
                    'created_day' => $expense->status->updated_at->format('Y-m-d'),
                    'now' => now()->format('Y-m-d')
                ]);
            }
        }
        else
        {
            Log::stack(['stack'])->error("[ExpenseManagerService:send] EMSs01: Can't instantiate model expense", [
                'expense_id' => $id_or_model_expense,
            ]);
        }


        return $is_send;
    }

    public function test_me()
    {
        return 'expense_manager';
    }

    public function unpaid($id_or_model_expense): bool
    {
        $is_done = false;

        $expense = $this->_instantiate_if_id($id_or_model_expense, Expense::class);

        if( $expense )
        {
            $this->add_status('unpaid', $expense);

            $is_done = true;
        }
        else
        {
            Log::stack(['stack'])->error("[ExpenseManagerService:unpaid] EMSw01: Can't instantiate model expense", [
                'expense_id' => $id_or_model_expense,
            ]);
        }


        return $is_done;
    }

    public function waiting($id_or_model_expense): bool
    {
        $is_done = false;

        $expense = $this->_instantiate_if_id($id_or_model_expense, Expense::class);

        if( $expense )
        {
            $this->add_status('waiting', $expense);
            $is_done = true;
        }
        else
        {
            Log::stack(['stack'])->error("[ExpenseManagerService:waiting] EMSw01: Can't instantiate model expense", [
                'expense_id' => $id_or_model_expense,
            ]);
        }


        return $is_done;
    }
}
