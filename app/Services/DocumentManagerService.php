<?php
namespace App\Services;


use App\Models\DocumentAvailableType;
use App\Models\Expense;
use App\Models\ExpenseDocument;
use App\Models\Document;
use App\Traits\UtilService;
use Illuminate\Database\Eloquent\Model;

class DocumentManagerService implements ICreateManager
{
    use UtilService;


    public function add_to_expense($id_or_model_document, $id_or_model_expense)
    {
        $expense_document = null;

        $document = $this->_instantiate_if_id($id_or_model_document, Document::class);
        $expense = $this->_instantiate_if_id($id_or_model_expense, Expense::class);

        if( $document && $expense )
        {
            $already_exist = ExpenseDocument::where('document_id', $document->id)
                ->where('expense_id', $expense->id)
                ->first();

            if( ! $already_exist )
            {
                $expense_document = new ExpenseDocument();
                $expense_document->expense_id = $expense->id;
                $expense_document->document_id = $document->id;
                $expense_document->save();
            }
        }
        else
        {
            // TODO
        }

        return $expense_document;
    }

    public function create(array $values): Model|null
    {
        $document = null;

        $filename = array_key_exists('filename', $values) ? $values['filename'] : false;
        $extension = array_key_exists('extension', $values) ? $values['extension'] : false;
        $path = array_key_exists('path', $values) ? $values['path'] : false;
        $type_identity = array_key_exists('type_identity', $values) ? $values['type_identity'] : false;

        $type = DocumentAvailableType::where('identity', $type_identity)->first();
        $path = str_replace(storage_path('app/'), '',  $path);

        $already_exist = Document::where('filename', $filename)
            ->where('path', $path)->first();

        if( $already_exist )
        {
            $already_exist->extension = $extension;
            $already_exist->type_id = $type->id;
            $already_exist->save();

            $document = $already_exist;
        }
        else
        {
            $document = new Document();
            $document->filename = $filename;
            $document->extension = $extension;
            $document->path = $path;
            $document->type_id = $type->id;

            $document->save();
        }

        return $document;
    }

    public function create_available_type(array $values)
    {
        $type = null;

        // check
        $label = array_key_exists('label', $values) ? $values['label'] : false;
        $code = array_key_exists('code', $values) ? $values['code'] : false;

        $identity = array_key_exists('identity', $values) ? $values['identity'] : null;
        $template = array_key_exists('template', $values) ? $values['template'] : null;
        $description = array_key_exists('description', $values) ? $values['description'] : null;

        if ($label && $code)
        {
            $type = new DocumentAvailableType([
                'label' => $label,
                'identity' => $identity,
                'code' => $code,
                'template' => $template,
                'description' => $description,
            ]);
            $type->save();
        }
        else
        {
            // TODO log
            dd('rERR');
        }

        return $type;
    }

    public function get_type(string $document_type_identity)
    {
       $type = DocumentAvailableType::where('identity', $document_type_identity)->first();

       return $type;
    }

    public function test_me()
    {
        return 'document_manager';
    }
}
