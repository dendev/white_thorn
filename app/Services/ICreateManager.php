<?php

namespace App\Services;

use Illuminate\Database\Eloquent\Model;

interface ICreateManager
{
    public function create(array $values): Model|null;
}
