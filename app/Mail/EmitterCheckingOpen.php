<?php

namespace App\Mail;

use App\Models\Expense;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;

class EmitterCheckingOpen extends Mailable
{
    use Queueable, SerializesModels;

    public string $content;
    public Expense $expense;

    /**
     * Create a new message instance.
     */
    public function __construct(array $args)
    {
        $this->expense = array_key_exists('expense', $args) ? $args['expense'] : null;
    }

    /**
     * Get the message envelope.
     */
    public function envelope(): Envelope
    {
        return new Envelope(
            subject: __('mail.emitter_checking_open_subject'),
        );
    }

    /**
     * Get the message content definition.
     */
    public function content(): Content
    {
        $this->content = __('mail.emitter_checking_open_content', [
            'type' => $this->expense->type->label,
            'date' => $this->expense->accounting_date->format('d/m/Y'),
            'receiver_name' => $this->expense->receiver->name,
            'amount' => $this->expense->amount,
        ]);

        return new Content(
            view: 'mails.emitter_checking_open',
        );
    }

    /**
     * Get the attachments for the message.
     *
     * @return array<int, \Illuminate\Mail\Mailables\Attachment>
     */
    public function attachments(): array
    {
        return [];
    }
}
