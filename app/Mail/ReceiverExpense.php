<?php

namespace App\Mail;

use App\Models\Expense;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Attachment;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;

class ReceiverExpense extends Mailable
{
    use Queueable, SerializesModels;

    public string $content;
    public Expense $expense;
    public mixed $pdf;

    /**
     * Create a new message instance.
     */
    public function __construct(array $args)
    {
        $this->expense = array_key_exists('expense', $args) ? $args['expense'] : null;
        $this->pdf = array_key_exists('pdf', $args) ? $args['pdf'] : null;
    }

    /**
     * Get the message envelope.
     */
    public function envelope(): Envelope
    {
        return new Envelope(
            subject: __('mail.receiver_expense_subject'),
        );
    }

    /**
     * Get the message content definition.
     */
    public function content(): Content
    {
        $this->content = __('mail.receiver_expense_content', [
            'type' => $this->expense->type->label,
            'date' => $this->expense->accounting_date->format('d/m/Y'),
            'emitter_name' => $this->expense->emitter->name,
            'emitter_bank_account' => $this->expense->emitter->bank_account,
            'amount' => $this->expense->amount,
        ]);

        return new Content(
            view: 'mails.receiver_expense',
        );
    }

    /**
     * Get the attachments for the message.
     *
     * @return array<int, \Illuminate\Mail\Mailables\Attachment>
     */
    public function attachments(): array
    {
        $attachments = [];

        if( $this->pdf)
        {
            $expense_report = Attachment::fromPath($this->pdf)
                //->as('name.pdf')
                ->withMime('application/pdf');
            $attachments[] = $expense_report;
        }

        return $attachments;
    }
}
