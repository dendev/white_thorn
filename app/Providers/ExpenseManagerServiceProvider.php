<?php

namespace App\Providers;

use App\Services\ExpenseManagerService;
use Illuminate\Support\ServiceProvider;

class ExpenseManagerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->singleton( 'expense_manager', function ($app) {
            return new ExpenseManagerService();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
    }
}
