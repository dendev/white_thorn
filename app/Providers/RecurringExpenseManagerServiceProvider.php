<?php

namespace App\Providers;

use App\Services\RecurringExpenseManagerService;
use Illuminate\Support\ServiceProvider;

class RecurringExpenseManagerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->singleton( 'recurring_expense_manager', function ($app) {
            return new RecurringExpenseManagerService();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
    }
}
