<?php

namespace App\Providers;

use App\Services\EmitterManagerService;
use Illuminate\Support\ServiceProvider;

class EmitterManagerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->singleton( 'emitter_manager', function ($app) {
            return new EmitterManagerService();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
    }
}
