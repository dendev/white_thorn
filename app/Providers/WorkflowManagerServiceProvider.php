<?php

namespace App\Providers;

use App\Services\WorkflowManagerService;
use Illuminate\Support\ServiceProvider;

class WorkflowManagerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->singleton( 'workflow_manager', function ($app) {
            return new WorkflowManagerService();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
    }
}
