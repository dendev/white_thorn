<?php

namespace App\Providers;

use App\Services\PdfManagerService;
use Illuminate\Support\ServiceProvider;

class PdfManagerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->singleton( 'pdf_manager', function ($app) {
            return new PdfManagerService();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
    }
}
