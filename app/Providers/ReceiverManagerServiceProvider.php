<?php

namespace App\Providers;

use App\Services\ReceiverManagerService;
use Illuminate\Support\ServiceProvider;

class ReceiverManagerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->singleton( 'receiver_manager', function ($app) {
            return new ReceiverManagerService();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
    }
}
