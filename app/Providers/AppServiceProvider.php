<?php

namespace App\Providers;

use App\Models\User;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        Gate::define('use-translation-manager', function (?User $user) {
            // Your authorization logic
            return $user !== null;
        });
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        // set https or not
        //https://stackoverflow.com/questions/47100382/urlforceschema-not-working-before-login
        if (env('HTTPS_ENABLED', false))
            $this->app['request']->server->set('HTTPS', true);

        // to fr date
        Blade::directive('datefr', function ($expression) {
            return "<?php if( ! empty( $expression ) && ! is_null( $expression ) ) echo ($expression)->format('d/m/Y'); ?>";
        });

        Blade::directive('datefr_with_time', function ($expression) {
            return "<?php if( ! empty( $expression ) && ! is_null( $expression ) ) echo ($expression)->format('d/m/Y H:i:s'); ?>";
        });
    }
}
