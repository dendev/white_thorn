<?php

namespace App\Providers;

use App\Services\DocumentManagerService;
use Illuminate\Support\ServiceProvider;

class DocumentManagerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->singleton( 'document_manager', function ($app) {
            return new DocumentManagerService();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
    }
}
