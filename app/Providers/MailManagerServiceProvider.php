<?php

namespace App\Providers;

use App\Services\MailManagerService;
use Illuminate\Support\ServiceProvider;

class MailManagerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->singleton( 'mail_manager', function ($app) {
            return new MailManagerService();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
    }
}
