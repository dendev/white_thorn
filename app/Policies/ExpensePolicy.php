<?php

namespace App\Policies;

use App\Models\Expense;
use App\Models\User;
use Illuminate\Auth\Access\Response;

class ExpensePolicy
{
   public function before(User $user, $ability): ?bool
    {
        if( $user->name === 'admin' )
            return true;
        return null;
    }

    /**
     * Determine whether the user can view any models.
     */
    public function viewAny(User $user): bool
    {
        //
        return true;
    }

    /**
     * Determine whether the user can view the model.
     */
    public function view(User $user, Expense $expense): bool
    {
        return true;
    }

    /**
     * Determine whether the user can create models.
     */
    public function create(User $user): bool
    {
        return true;
    }

    /**
     * Determine whether the user can update the model.
     */
    public function update(User $user, Expense $expense): bool
    {
        return true;
    }

    /**
     * Determine whether the user can delete the model.
     */
    public function delete(User $user, Expense $expense): bool
    {
        return true;
    }

    /**
     * Determine whether the user can restore the model.
     */
    public function restore(User $user, Expense $expense): bool
    {
        return true;
    }

    /**
     * Determine whether the user can permanently delete the model.
     */
    public function forceDelete(User $user, Expense $expense): bool
    {
        return true;
    }

    // transitions
    public function toCreated(User $user): bool
    {
        $can = true;

        return $can;
    }

    public function createdToSend(User $user): bool
    {
        $can = true;

        return $can;
    }

    public function sendToWaiting(User $user): bool
    {
        $can = true;

        return $can;
    }

    public function waitingToChecking(User $user): bool
    {
        $can = true;

        return $can;
    }

    public function checkingToPaid(User $user): bool
    {
        $can = true;

        return $can;
    }

    public function checkingToUnpaid(User $user): bool
    {
        $can = true;

        return $can;
    }

    public function unpaidToSend(User $user): bool
    {
        $can = true;

        return $can;
    }


    //
    public function verify(User $user, Expense $expense): bool
    {
        $can = false;

        if( $expense->check_status_identity('checking'))
            $can = true;

        return $can;
    }
}
