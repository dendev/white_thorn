#!/bin/bash

./generate_doc.sh
./generate_coverage.sh
./generate_metrics.sh

if [ "$1" == '-o' ] || [ "$1" == '--open' ]
then
    xdg-open ./docs/index.html;
fi;



exit $?;
